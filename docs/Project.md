
# Project

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | id |  [optional]
**name** | **String** | name |  [optional]
**fullName** | **String** | fullName |  [optional]
**department** | **String** | department |  [optional]
**members** | [**List&lt;Assignation&gt;**](Assignation.md) | members |  [optional]




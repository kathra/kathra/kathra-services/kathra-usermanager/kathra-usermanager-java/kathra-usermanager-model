
# Department

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | id |  [optional]
**name** | **String** | name |  [optional]
**fullName** | **String** | fullName |  [optional]
**members** | [**List&lt;Assignation&gt;**](Assignation.md) | members |  [optional]
**projects** | [**List&lt;Project&gt;**](Project.md) | projects |  [optional]




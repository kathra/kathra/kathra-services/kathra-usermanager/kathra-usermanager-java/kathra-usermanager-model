
# User

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **String** | id |  [optional]
**username** | **String** | username |  [optional]
**firstName** | **String** | firstName |  [optional]
**lastName** | **String** | lastName |  [optional]
**email** | **String** | email |  [optional]
**password** | **String** | password |  [optional]
**phone** | **String** | phone |  [optional]
**projects** | [**List&lt;Assignation&gt;**](Assignation.md) | projects |  [optional]



